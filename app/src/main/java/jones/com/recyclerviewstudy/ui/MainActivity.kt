package jones.com.recyclerviewstudy.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import jones.com.recyclerviewstudy.R

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private val myadapter: MyAdapter = MyAdapter(arrayOf("item1", "item2", "item3", "item4"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.list)

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayout.VERTICAL, false)
            addItemDecoration(DividerItemDecoration(this@MainActivity, LinearLayout.VERTICAL))
            adapter = myadapter
        }

    }

}

